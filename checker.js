#!/usr/bin/env node

var fs = require('fs');
var csvParser = require('babyparse');
var async = require('async');

var grouper = require('./src/datagrouper');
var Workday = require('./src/workday');
var GeocoderQueue = require('./src/geocoder/queue');

var gpsToString = require('./src/geocoder/utils').gpsToString;
var gpsFromString = require('./src/geocoder/utils').gpsFromString;
var Geocoder = require('./src/geocoder/geocoder');
var CachedGeocoder = require('./src/geocoder/cachedGeocoder');
var Cache = require('./src/cache');
var distanceCalc = require('./src/distance');
var extract = require('./src/extractor');
var MapGenerator = require('./src/mapgenerator');
var utils = require('./src/utils');

var yargs = require('yargs')
	.usage('Usage: $0 --limit [number] --apikey [key] --file [file]')
	.demand(['limit', 'file'])
	.demand(['limit', 'file'])
	.default({ d: 500})
	.alias('f', 'file').describe('f', 'Path to csv file')
	.alias('l', 'limit').describe('l', 'Distance limit')
	.alias('k', 'apikey').describe('k', 'Google api key')
	.alias('d', 'delay').describe('d', 'Geocoder delay');

var args = yargs.argv;

try {
	var csv = fs.readFileSync(args.file, 'utf8');
} catch (e) {
	yargs.showHelp();
	console.log('File read error');
	process.exit(1);
}

var mapGenerator = new MapGenerator(args.apikey);
var geocoder = new CachedGeocoder(
	new Geocoder(args.apikey, args.delay),
	new Cache(__dirname + '/cache/geocode_'+ utils.getDailyFileName() +'.db')
);

var csvData = extract(csvParser.parse(csv, {delimiter: ';'}).data, function(row) {
	var address = (row[5] || '').trim();
	var gps = (row[4] || '').trim();
	return (gps !== '' && address !== '');
});
var header = csvData.header;
var invalidRows = csvData.invalid;

var workdays = grouper(csvData.data, ['line', 'district', 'name', 'date']).map(function(i) {
	return new Workday(i.key, i.vals);
});

var counter = 0;
var visitsLength = workdays.reduce(function(memo, workday) {
	memo += workday.visits.length;
	return memo;
}, 0);

async.mapSeries(workdays, function(workday, doneWithWorkday) {

	var geocodedWorkday = new Workday(workday, []);
	var geocoderQueue = new GeocoderQueue(workday.visits, geocoder);

	geocoderQueue.on('geocode', function(gpsObj, visit) {
		var visitGpsObj = gpsFromString(visit.gps);
		var distance = distanceCalc(visitGpsObj, gpsObj);

		if (distance > args.limit) {
			visit.geocodedAddress = gpsToString(gpsObj);
			visit.distance = distance;
			geocodedWorkday.visits.push(visit);
		}

		console.log('[%s/%s] [%s] %s - %s [%s]',
			++counter,
			visitsLength,
			distance,
			visit.gps,
			gpsToString(gpsObj),
			visit.address
		);
	});

	geocoderQueue.on('error', function(e, visit) {
		var c = ++counter;
		if (e.message === 'Zero Results') {
			console.error('[%s/%s] Geocode error', c, visitsLength, e.message, visit.address);
		} else {
			console.error("error when geocoding row", workday.toSingleRow(visit));
			console.error(e.message);
			console.error(e.stack);
		}
		invalidRows.push(workday.toSingleRow(visit));
	});

	geocoderQueue.on('end', function() {
		if (geocodedWorkday.visits.length) {
			mapGenerator.generate(geocodedWorkday.visits, function(e, imgData) {
				if (e) {
					console.error('Map Generate Error');
					console.error(e.message);
					console.error(e.stack);
				}
				fs.writeFileSync(__dirname + '/output-maps/'+ geocodedWorkday.toFilename() +'.png', imgData, 'binary');
				doneWithWorkday(null, geocodedWorkday);
			});
		} else {
			doneWithWorkday(null, geocodedWorkday);
		}
	});

	geocoderQueue.geocode();

}, function(err, workdaysWithDistance) {

	invalidRows.unshift(header);
	invalidRows = csvParser.unparse(invalidRows, {delimiter: ';'});

	var rows = [];
	workdaysWithDistance.filter(function(wd) {
		return wd.visits.length > 0;
	}).forEach(function(wd) {
		rows = rows.concat(wd.toGeocodedVisitRows());
	});

	var noteHeader = header.pop();
	rows.unshift(header.concat(['gps adresu', 'odległość od punktów', noteHeader]));
	rows = csvParser.unparse(rows, {delimiter: ';'});

	fs.writeFileSync(__dirname + '/output/out.csv', rows);
	fs.writeFileSync(__dirname + '/output/invalid.csv', invalidRows);

});
