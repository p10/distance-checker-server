## Instalacja

	$ git clone git@bitbucket.org:p10/distance-checker-server.git
	$ cd distance-checker-server
	$ npm install


## Użycie

	$ /path/to/node /path/to/distance-checker-server/checker.js --limit [num] --apikey [klucz google api] --file [input csv]

	$ /path/to/node /path/to/distance-checker-server/distance.js --apikey [klucz google api] --file [input csv]


W [konsoli google](https://console.developers.google.com) musi być włączone:
	- Geocoding API
	- Static Maps API