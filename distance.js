#!/usr/bin/env node

var fs = require('fs');
var csvParser = require('babyparse');
var async = require('async');
var grouper = require('./src/datagrouper');
var Workday = require('./src/workday');
var Geocoder = require('./src/geocoder/geocoder');
var CachedGeocoder = require('./src/geocoder/cachedGeocoder');
var Cache = require('./src/cache');
var gpsToString = require('./src/geocoder/utils').gpsToString;
var DistanceCalculator = require('./src/distanceCalculator');
var extract = require('./src/extractor');
var utils = require('./src/utils');

var yargs = require('yargs')
	.usage('Usage: $0 --apikey [key] --file [file]')
	.demand(['file'])
	.default({ d: 500})
	.alias('f', 'file').describe('f', 'Path to csv file')
	.alias('k', 'apikey').describe('k', 'Google api key')
	.alias('d', 'delay').describe('d', 'Geocoder delay');

var args = yargs.argv;

try {
	var csv = fs.readFileSync(args.file, 'utf8');
} catch (e) {
	yargs.showHelp();
	console.log('File read error');
	process.exit(1);
}

var geocoder = new CachedGeocoder(
	new Geocoder(args.apikey, args.delay),
	new Cache(__dirname + '/cache/geocode_'+ utils.getDailyFileName() +'.db')
);
var csvData = extract(csvParser.parse(csv, {delimiter: ';'}).data, function(row) {
	var address = (row[5] || '').trim();
	var gps = (row[4] || '').trim();
	return (gps !== '' || address !== '');
});
var header = csvData.header;
var invalidRows = csvData.invalid;

var workdays = grouper(csvData.data, ['line', 'district', 'name', 'date']).map(function(i) {
	return new Workday(i.key, i.vals);
}).filter(function(workday) {
	if (workday.visits.length < 2) {
		// we can measure distance between at least 2 visits
		invalidRows = invalidRows.concat(workday.toRows());
		return false;
	}
	return true;
});

async.mapSeries(workdays, function(workday, doneWithWorkday) {

	var calculator = new DistanceCalculator(workday.visits, geocoder);

	calculator.on('geocode', function(gpsPoint, visit) {
		console.log('Geocode %s [%s]', gpsToString(gpsPoint), visit.address);
	});

	calculator.on('geocode-error', function(e, visit) {
		console.error('Geocode Error', e.message, visit.address);
		if (e.message !== 'Zero Results') {
			console.error(e.stack);
		}
		invalidRows.push(workday.toSingleRow(visit));
	});

	calculator.on('distance', function(distance) {
		console.log('Distance %s for %s', distance, workday.name);
		workday.distance = distance;
		doneWithWorkday(null, workday);
	});

	calculator.calculate();

}, function(err, workdaysWithDistance) {

	invalidRows.unshift(header);
	invalidRows = csvParser.unparse(invalidRows, {delimiter: ';'});

	workdaysWithDistance = workdaysWithDistance.map(function(workday) {
		return workday.toDistnaceRow();
	});
	header.length = 4;
	workdaysWithDistance.unshift(header.concat(['dystans w metrach']));
	workdaysWithDistance = csvParser.unparse(workdaysWithDistance, {delimiter: ';'});

	fs.writeFileSync(__dirname + '/output/distance-out.csv', workdaysWithDistance);
	fs.writeFileSync(__dirname + '/output/distance-invalid.csv', invalidRows);

});