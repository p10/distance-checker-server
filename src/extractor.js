function extractor(data, filter) {
	var header = [], invalidRows = [];
	var filtered = data.filter(function(row, index) {
		if (index == 0) {
			header = row;
			return false;
		}
		if (filter(row)) {
			return true;
		} else {
			invalidRows.push(row);
			return false;
		}
	}).map(function(row) {
		var datetime = row[2].trim().split(' ');
		return {
			line: row[0],
			district: row[1],
			date: datetime[0],
			name: row[3],
			gps: row[4],
			time: datetime[1],
			address: row[5],
			note: row[6]
		};
	});
	return {
		header: header,
		invalid: invalidRows,
		data: filtered
	};
}

module.exports = extractor;