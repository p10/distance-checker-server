var EventEmitter = require('events').EventEmitter;
var util = require('util');
var async = require('async');
var geocode = require('./geocoder/geocoder').geocode;
var gpsFromString = require('./geocoder/utils').gpsFromString;
var distanceCalc = require('./distance');

function DistanceCalculator(visits, geocoder) {
	this.visits = visits;
	this.geocoder = geocoder;
}

module.exports = DistanceCalculator;
util.inherits(DistanceCalculator, EventEmitter);

DistanceCalculator.prototype.calculate = function() {
	var self = this;
	async.mapSeries(self.visits, function(visit, doneVisit) {
		if (visit.gps !== '') {
			return doneVisit(null, gpsFromString(visit.gps));
		}
		self.geocoder.geocode(visit.address, function(e, gpsPoint) {
			if (e) {
				self.emit('geocode-error', e, visit);
				return doneVisit(null, null);
			} else {
				self.emit('geocode', gpsPoint, visit);
				return doneVisit(null, gpsPoint);
			}
		});
	}, function(e, gpsVisits) {
		gpsVisits = gpsVisits.filter(function(gps) {
			return gps !== null;
		});
		var distance = 0;
		for (var i = 0; i < gpsVisits.length - 1; i += 1) {
			distance += distanceCalc(gpsVisits[i], gpsVisits[i + 1]);
		}
		self.emit('distance', distance);
	});
};