function Workday(props, visits) {
	this.line = props.line;
	this.district = props.district;
	this.date = props.date;
	this.name = props.name;
	this.visits = visits;
	this.distance = 0;
}

Workday.prototype.toRows = function() {
	return this.visits.map(this.toSingleRow.bind(this));
};

Workday.prototype.toSingleRow = function(visit) {
	return [
		this.line,
		this.district,
		this.date + ' ' + visit.time,
		this.name,
		visit.gps,
		visit.address,
		visit.note
	];
};

Workday.prototype.toSingleGeocodedVisitRow = function (visit) {
	return [
		this.line,
		this.district,
		this.date + ' ' + visit.time,
		this.name,
		visit.gps,
		visit.address,
		visit.geocodedAddress,
		visit.distance,
		visit.note
	];
};

Workday.prototype.toDistnaceRow = function() {
	return [
		this.line,
		this.district,
		this.date,
		this.name,
		this.distance
	];
};

Workday.prototype.toGeocodedVisitRows = function() {
	return this.visits.map(this.toSingleGeocodedVisitRow.bind(this));
};

Workday.prototype.toFilename = function() {
	return [this.line, this.district, this.date, this.name.replace(' ', '_')].join('_');
};

module.exports = Workday;