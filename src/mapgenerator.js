var gm = require('googlemaps');

function MapGenerator(apiKey) {
	if (apiKey) {
		gm.config('console-key', apiKey);
	}
}

MapGenerator.prototype.generate = function(visits, cb) {
	var markers = [], paths = [];

	visits.forEach(function(v, index) {
		markers.push({location: v.gps, color: '0xfff000', label: index + 1});
		markers.push({location: v.geocodedAddress, color: '0x7995FC', label: index + 1});
		paths.push({
			points: [v.gps, v.geocodedAddress]
		})
	});

	return gm.staticMap(
		null, null, '640x640',
		cb,
		null, null,
		markers,
		null,
		paths);
};

module.exports = MapGenerator;
