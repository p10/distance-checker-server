var Datastore = require('nedb');

function Cache(filename) {
	this.db = new Datastore({ filename: filename, autoload: true });
}

Cache.prototype.find = function(key, cb) {
	this.db.findOne({_id: key}, function(e, doc) {
		if (e) {
			return cb(new Error('Cache find error. ' + e.message));
		}
		if (!doc) {
			return cb(null, null);
		}
		cb(null, doc.val);
	});
};

Cache.prototype.insert = function(key, value, cb) {
	this.db.update({_id: key}, {val: value}, {upsert: true}, function(e, num, upserted) {
		if (e) {
			return cb(new Error('Cache insert error. ' + e.message));
		}
		if (num !== 1) {
			return cb(new Error('Cache insert faild. Efected rows ' + num));
		}
		cb();
	})
};

module.exports = Cache;