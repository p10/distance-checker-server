var gm = require('googlemaps');

function Geocoder(apiKey, delay) {
	this.delay = delay;
	if (apiKey) {
		gm.config('console-key', apiKey);
	}
}

Geocoder.prototype.geocode = function(address, callback) {
	var timeout = this.timeout;
	gm.geocode(address, function (e, response) {
		setTimeout(function() {
			if (e) {
				return callback(e);
			}
			if (response.status === "OK") {
				callback(null, response.results[0].geometry.location);
			} else if (response.status === "ZERO_RESULTS") {
				callback(new Error('Zero Results'));
			} else {
				callback(new Error("Geogode Error " + response.error_message || response.status));
			}
		}, timeout);
	});
};

module.exports = Geocoder;

