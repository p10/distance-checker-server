exports.gpsToString = function(point) {
	return [point.lat, point.lng].join(',');
};

exports.gpsFromString = function(gpsString) {
	var data = gpsString.split(',');
	return {lat: parseFloat(data[0]), lng: parseFloat(data[1])};
};