var EventEmitter = require('events').EventEmitter;
var util = require('util');

function GeocoderQueue(jobs, geocoder) {
	this.jobs = jobs;
	this.geocoder = geocoder;
}

module.exports = GeocoderQueue;
util.inherits(GeocoderQueue, EventEmitter);

GeocoderQueue.prototype.geocode = function() {
	if (this.executed) {
		return;
	}
	this.executed = true;
	var job = this.jobs.shift();
	var self = this;
	self.geocoder.geocode(job.address, function(e, gps) {
		if (e) {
			self.emit('error', e, job);
		} else {
			self.emit('geocode', gps, job);
		}
		if (self.jobs.length) {
			self.executed = null;
			self.geocode();
		} else {
			self.emit('end');
		}
	});
};
