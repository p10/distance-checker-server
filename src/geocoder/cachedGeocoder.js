var debug = require('debug')('dc:cachedGeocoder');

function CachedGeocoder(geocoder, cache) {
	this.geocoder = geocoder;
	this.cache = cache;
}

CachedGeocoder.prototype.geocode = function(address, callback) {
	var self = this;
	this.cache.find(address, function(e, cachedGps) {
		if (e) {
			return callback(e);
		}
		if (cachedGps) {
			debug('returnig gps from cache');
			return callback(null, cachedGps, true);
		} else {
			debug('getting gps from geocoder');
			self.geocoder.geocode(address, function(e, gpsObj) {
				if (e) {
					return callback(e);
				}
				self.cache.insert(address, gpsObj, function(e) {
					callback(null, gpsObj);
				})
			});
		}
	});
};

module.exports = CachedGeocoder;